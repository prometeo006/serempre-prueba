import {LoggingWinston} from "@google-cloud/logging-winston";
import * as winston from "winston";

const format = winston.format.combine(
    winston.format.timestamp({format: "YYYY-MM-DD HH:mm:ss:ms"}),
    winston.format.colorize({all: true}),
    winston.format.printf(
        (info) => `${info.timestamp} ${info.level}: ${info.message}`,
    ),
);

const transports = [
  new winston.transports.Console({level: "debug"}),
  new LoggingWinston(),
];

export const Logger = winston.createLogger({
  format,
  transports,
});
