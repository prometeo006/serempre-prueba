import {Router as router} from "express";
import {UserController, PointController} from "../controllers";

/**
 * Class for express routes
 */
export class ExpressConfig {
  /**
   * This method return the routes configured
   * @return {router} Routes for express
   */
  public getRoutes(): router {
    const routes = router();
    routes.use("/users", this.userRoutes());
    routes.use("/points", this.pointRoutes());
    return routes;
  }

  /**
   * This method return the users routes
   * @return {router} Routes for express
   */
  private userRoutes(): router {
    const userController = new UserController();
    const usersRouter = router();
    usersRouter.get("/", userController.read);
    usersRouter.get("/:id", userController.read);
    usersRouter.post("/", userController.create);
    usersRouter.put("/:id", userController.update);
    usersRouter.delete("/:id", userController.delete);
    return usersRouter;
  }

  /**
   * This method return the points routes
   * @return {router} Routes for express
   */
  private pointRoutes(): router {
    const pointController = new PointController();
    const pointsRouter = router();
    pointsRouter.get("/", pointController.read);
    pointsRouter.get("/:id", pointController.read);
    pointsRouter.post("/:idUser", pointController.create);
    pointsRouter.put("/:idUser/:id", pointController.update);
    pointsRouter.delete("/:idUser/:id", pointController.delete);
    return pointsRouter;
  }
}
