export interface Repository<Type> {
    add(params: Type): Promise<Type>,
    findAll(): Promise<Type[]>,
    find(id: string): Promise<Type>,
    update(id: string, params: any): Promise<Type>,
    delete(id: string): Promise<boolean>
}
