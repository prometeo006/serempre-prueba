import {firestore} from "firebase-admin";
import {Logger} from "../config";
import {Points} from "../entities";
import {Repository} from "./repository";

/**
 * Class for database operation with points
 */
export class PointRepository implements Repository<Points> {
  private index: firestore.DocumentReference;
  private collection: firestore.CollectionReference;

  /**
   * Class constructor
   */
  constructor() {
    this.collection = firestore().collection("users");
    this.index = firestore().collection("params").doc("pointCounter");
  }

  /**
   * Method for add a point
   * @param {Points} point object to be added
   * @return {Promise<Points>} object added
   */
  public async add(point: Points): Promise<Points> {
    try {
      const {idx: indexValue} = (await this.index.get()).data() || {idx: 0};
      point.id = indexValue;
      const idUser = point.idUser || "";
      delete point.idUser;
      const response = await this.collection.doc(idUser).collection("points")
          .doc(String(indexValue)).create(point);
      this.index.set({idx: firestore.FieldValue.increment(10)}, {merge: true});
      Logger.debug("point-repository.add " + JSON.stringify(response));
      return point;
    } catch (error) {
      Logger.error("point-repository.add", error);
      throw error;
    }
  }

  /**
   * Method for find all points
   * @return {Promise<Points>[]} List of all points
   */
  public async findAll(): Promise<Points[]> {
    try {
      const resultado: Points[] = [];
      const pointsList = await firestore()
          .collectionGroup("points").get();
      pointsList.forEach((doc) => {
        const {id, quantity, reason} = doc.data();
        resultado.push({id, quantity, reason});
      });
      return resultado;
    } catch (error) {
      Logger.error("point-repository.findAll", error);
      throw error;
    }
  }

  /**
   * Method for find specific point
   * @param {string} searchId id of point for looking for
   * @return {Promise<Points>} point finded
   */
  public async find(searchId: string): Promise<Points> {
    try {
      let resultado: Points = {} as Points;
      const pointsList = await firestore().collectionGroup("points")
          .where("id", "==", Number(searchId)).get();
      pointsList.forEach((doc) => {
        const {id, quantity, reason} = doc.data();
        resultado = {id, quantity, reason};
      });
      return resultado;
    } catch (error) {
      Logger.error("point-repository.find", error);
      throw error;
    }
  }

  /**
   * Method por update a point
   * @param {string} id id of point to be updated
   * @param {Points} params params for update
   * @return {Promise<Points>} point updated
   */
  public async update(id: string, params: Points): Promise<Points> {
    try {
      const idUsr = params.idUser || "";
      delete params.idUser;
      const result = await this.collection.doc(idUsr).collection("points")
          .doc(id).update(params);
      Logger.debug("point-repository.update" + JSON.stringify(result) );
      return params;
    } catch (error) {
      Logger.error("point-repository.update", error);
      throw error;
    }
  }

  /**
   * Method for delete a point
   * @param {string} searchId id point to be deleted
   * @return {Promise<boolean>} true if the point was deleted
   */
  public async delete(searchId: string): Promise<boolean> {
    try {
      const point = await this.collection.doc(searchId).delete();
      Logger.debug("point-repository.delete: " + JSON.stringify(point));
      return true;
    } catch (error) {
      Logger.error("point-repository.delete", error);
      throw error;
    }
  }
}
