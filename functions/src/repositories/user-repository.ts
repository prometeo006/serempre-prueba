import {firestore} from "firebase-admin";
import {Logger} from "../config";
import {Users} from "../entities";
import {Repository} from "./repository";

/**
 * Class for database operation with users 
 */
export class UserRepository implements Repository<Users> {
  private index: firestore.DocumentReference;
  private collection: firestore.CollectionReference<Users>;
  readonly hidePassword = "**********";

  /**
   * Class constructor
   */
  constructor() {
    this.collection = firestore().collection("users") as
    firestore.CollectionReference<Users>;
    this.index = firestore().collection("params").doc("userCounter");
  }

  /**
   * Method for add user
   * @param {Users} user user to be added
   * @return {Promise<Users>} object added
   */
  public async add(user: Users): Promise<Users> {
    try {
      const exists = await this.collection
          .where("email", "==", user.email).get();
      if (!exists.empty) {
        throw new Error("User already exists");
      }
      const {idx: indexValue} = (await this.index.get()).data() || {idx: 0};
      user.id = indexValue;
      const response = await this.collection
          .doc(user.id.toString()).create(user);
      this.index.set({idx: firestore.FieldValue.increment(10)}, {merge: true});
      Logger.debug("user-repository.add " + JSON.stringify(response));
      return user;
    } catch (error) {
      Logger.error("user-repository.add", error);
      throw error;
    }
  }

  /**
   * Method for find all users
   * @return {Promise<Users>[]} list of all users
   */
  public async findAll(): Promise<Users[]> {
    try {
      const resultado: Users[] = [];
      const usersCollection = await this.collection.get();
      usersCollection.forEach((doc) => {
        const {id, name, email} = doc.data();
        resultado.push({id, name, email, password: this.hidePassword});
      });
      return resultado;
    } catch (error) {
      Logger.error("user-repository.findAll", error);
      throw error;
    }
  }

  /**
   * Method for find specific user
   * @param {string} searchId id of user for looking for
   * @return {Promise<Users>} user finded
   */
  public async find(searchId: string): Promise<Users> {
    try {
      let resultado: Users = {} as Users;
      const usrCollection = await this.collection
          .where("id", "==", Number(searchId)).get();
      usrCollection.forEach((doc) => {
        const {id, name, email} = doc.data();
        resultado = {id, name, email, password: this.hidePassword};
      });
      return resultado;
    } catch (error) {
      Logger.error("user-repository.find", error);
      throw error;
    }
  }

  /**
   * Method por update a user
   * @param {string} id id of user to be updated
   * @param {string} params params for update
   * @return {Promise<Users>} user updated
   */
  public async update(id: string, params: Users): Promise<Users> {
    try {
      const result = await this.collection.doc(id).update(params);
      Logger.debug("user-repository.update" + JSON.stringify(result) );
      return params;
    } catch (error) {
      Logger.error("user-repository.update", error);
      throw error;
    }
  }

  /**
   * Method for delete a user
   * @param {number} id id point to be deleted
   * @return {Promise<boolean>} true if the user was deleted
   */
  public async delete(id: string): Promise<boolean> {
    try {
      const result = await this.collection.doc(id).delete();
      Logger.debug("user-repository.delete: " + JSON.stringify(result));
      return true;
    } catch (error) {
      Logger.error("user-repository.delete", error);
      throw error;
    }
  }
}
