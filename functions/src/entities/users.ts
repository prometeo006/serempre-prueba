import {Points} from "./points";

export interface Users{
    id: number,
    name: string,
    email: string,
    password: string,
    points?: Points
}
