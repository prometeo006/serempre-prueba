export interface Points{
    id: number,
    quantity: number,
    reason: string,
    idUser?: string
}
