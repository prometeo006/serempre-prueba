import * as cors from "cors";
import * as express from "express";
import * as functions from "firebase-functions";
import {ExpressConfig} from "./config";
import {initializeApp} from "firebase-admin";

const expressRoutes = new ExpressConfig();
const app = express();
app.use(cors());
app.use(expressRoutes.getRoutes());
initializeApp();

exports.api = functions.https.onRequest(app);

