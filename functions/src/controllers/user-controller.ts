import {createHash, randomBytes} from "crypto";
import {Request, Response} from "express";
import {Logger} from "../config";
import {Users} from "../entities";
import {Repository, UserRepository} from "../repositories";

/** This method create a new user
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handler
*/
function createUser(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {body} = req;
    const user: Users = {
      id: body.id,
      name: body.name,
      email: body.email,
      password: body.password,
    };
    const {salt, password} = generatePassword(user.password);
    user.password = password;
    const response = repository.add({...user, salt});
    response.then((_) => {
      Logger.debug("user-controller.createUser: user created");
      res.status(201).send({status: "OK"});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "User not created"});
    });
  } catch (error) {
    Logger.error("user-controller.createUser", error);
    res.status(500).send({error: "Error server"});
  }
}

/** This method search or list users
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handler
*/
function readUser(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {params} = req;
    let response;
    if (params.id) {
      response = repository.find(params.id);
    } else {
      response = repository.findAll();
    }
    response.then((users) => {
      Logger.debug("user-controller.readUser: users found");
      res.status(200).send({status: "OK", data: users});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "Cant list users"});
    });
  } catch (error) {
    Logger.error("user-controller.readUser", error);
    res.status(500).send({error: "Error server"});
  }
}

/** This method update a user
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handler
*/
function updateUser(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {params, body} = req;
    const {name, password: passwordClear} = body;
    if (!params.id) {
      res.status(400);
      return;
    }
    let parametersUpdate: any = {};
    if (name) {
      parametersUpdate = {...parametersUpdate, name};
    }
    if (passwordClear) {
      const {salt, password} = generatePassword(passwordClear);
      parametersUpdate = {...parametersUpdate, password, salt};
    }
    if (Object.entries(parametersUpdate).length === 0) {
      res.status(400);
      return;
    }
    const response = repository.update(params.id, parametersUpdate);
    response.then((_) => {
      Logger.debug("user-controller.updateUser: user update");
      res.status(200).send({status: "OK"});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "Cant update user"});
    });
  } catch (error) {
    Logger.error("user-controller.updateUser", error);
    res.status(500).send({error: "Error server"});
  }
}

/** This method delete a user
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handler
*/
function deleteUser(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {params} = req;
    if (!params.id) {
      res.status(400);
      return;
    }
    const response = repository.delete(params.id);
    response.then((_) => {
      Logger.debug("user-controller.deleteUser: user delete");
      res.status(200).send({status: "OK"});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "Cant delete user"});
    });
  } catch (error) {
    Logger.error("user-controller.deleteUser", error);
    res.status(500).send({error: "Error server"});
  }
}

/**
 * This method generate an object with the salt and the hashed password
 * @param {string} passwordClear the clear password
 * @return {{salt: string, password: string}} salt and hashed password
 */
function generatePassword(passwordClear: string) {
  const salt = randomBytes(6).toString("hex");
  const password = createHash("md5")
      .update(passwordClear + salt)
      .digest("hex");
  return {salt, password};
}

/**
 * class for user operations
 */
export class UserController {

  /**
   * Method create
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public create(req: Request, res: Response): void {
    const repository = new UserRepository();
    return createUser(req, res, repository);
  }

  /**
   * method read
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public read(req: Request, res: Response): void {
    const repository = new UserRepository();
    return readUser(req, res, repository);
  }

  /**
   * method update
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public update(req: Request, res: Response): void {
    const repository = new UserRepository();
    return updateUser(req, res, repository);
  }

  /**
   * method delete
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public delete(req: Request, res: Response): void {
    const repository = new UserRepository();
    return deleteUser(req, res, repository);
  }
}
