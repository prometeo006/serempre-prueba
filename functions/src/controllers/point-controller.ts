import {Request, Response} from "express";
import {Logger} from "../config";
import {Points} from "../entities";
import {PointRepository, Repository} from "../repositories";

/** This function create a point
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handler
*/
function createPoint(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {body, params} = req;
    const point: Points = {
      id: 0,
      quantity: body.quantity,
      reason: body.reason,
      idUser: params.idUser,
    };
    const response = repository.add(point);
    response.then((_) => {
      Logger.debug("point-controller.createPoint: point created");
      res.status(201).send({status: "OK"});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "Point not created"});
    });
  } catch (error) {
    Logger.error("point-controller.createPoint", error);
    res.status(500).send({error: "Error server"});
  }
}

/** This method search or list points
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handler
*/
function readPoint(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {params} = req;
    let response;
    if (params.id) {
      response = repository.find(params.id);
    } else {
      response = repository.findAll();
    }
    response.then((points) => {
      Logger.debug("point-controller.readPoint: points found");
      res.status(200).send({status: "OK", data: points});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "Cant list points"});
    });
  } catch (error) {
    Logger.error("point-controller.readPoint", error);
    res.status(500).send({error: "Error server"});
  }
}

/** This method update a point
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handler
*/
function updatePoint(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {params, body} = req;
    const {quantity, reason} = body;
    if (!params.id) {
      res.status(400);
      return;
    }
    let parametersUpdate: any = {idUser: params.idUser};
    if (quantity) {
      parametersUpdate = {...parametersUpdate, quantity};
    }
    if (reason) {
      parametersUpdate = {...parametersUpdate, reason};
    }
    if (Object.entries(parametersUpdate).length === 0) {
      res.status(400);
      return;
    }
    const response = repository.update(params.id, parametersUpdate);
    response.then((_) => {
      Logger.debug("point-controller.updatePoint: point update");
      res.status(200).send({status: "OK"});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "Cant update point"});
    });
  } catch (error) {
    Logger.error("point-controller.updatePoint", error);
    res.status(500).send({error: "Error server"});
  }
}

/** This method delete a point
 * @param {Request} req - http request
 * @param {Response} res - http response
 * @param {Repository} repository - repository handlervvv
*/
function deletePoint(req: Request, res: Response, repository: Repository<any>):
    void {
  try {
    const {params} = req;
    if (!params.id) {
      res.status(400);
      return;
    }
    const response = repository.delete(params.id);
    response.then((users) => {
      Logger.debug("point-controller.deletePoint: point deleted");
      res.status(200).send({status: "OK"});
    }).catch((_) => {
      res.status(400).send({status: "ERROR", error: "Cant delete point"});
    });
  } catch (error) {
    Logger.error("point-controller.deletePoint", error);
    res.status(500).send({error: "Error server"});
  }
}


/**
 * class for point operations
 */
export class PointController {
  /**
   * Method create
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public create(req: Request, res: Response): void {
    const repository = new PointRepository();
    return createPoint(req, res, repository);
  }

  /**
   * method read
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public read(req: Request, res: Response): void {
    const repository = new PointRepository();
    return readPoint(req, res, repository);
  }

  /**
   * method update
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public update(req: Request, res: Response): void {
    const repository = new PointRepository();
    return updatePoint(req, res, repository);
  }

  /**
   * method delete
   * @param {Request} req - http request
   * @param {Response} res - http response
   * @return {void} 
   */
  public delete(req: Request, res: Response): void {
    const repository = new PointRepository();
    return deletePoint(req, res, repository);
  }
}
